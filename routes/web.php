<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'App\Http\Controllers\PagesController@index');

Route::get('/hello', function () {
    return '<h1>Hello World</h1>';
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/users/{id}', function ($id) {
    return 'This is a user ' . $id;
});

//Patient

Route::get('/patient', 'App\Http\Controllers\PatientController@all')->name('expenses.all');
// Route::post('/patient', 'PatientController@store')->name('expenses.store');
Route::get('/patient/{email}', 'App\Http\Controllers\PatientController@show')->name('patient.show');
Route::put('/patient/{email}', 'App\Http\Controllers\PatientController@update')->name('patient.update');
Route::delete('/patient/{id}', 'App\Http\Controllers\PatientController@destroy')->name('expenses.destroy');


//Appointment
Route::post('/appointments', 'App\Http\Controllers\AppointmentsController@store')->name('appointments.store');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
