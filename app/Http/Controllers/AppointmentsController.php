<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointments;

class AppointmentsController extends Controller
{
    //
    public function store(Request $request){
        
        $appointment = new Appointments();
        $appointment->patient_email = $request->input("patient_email");
        $appointment->doctor = $request->input("doctor");
        $appointment->date_appointment = $request->input("date_appointment");
        $appointment->save();
        
        return response()->json([
            'message' => 'appointment created!',
            'appointment' => $appointment
        ]);
    }
}
