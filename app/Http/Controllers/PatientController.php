<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Patient;

class PatientController extends Controller
{
    //
    public function index()
    {
        $patients = Patients::all();
        return response()->json($patients);
    }

    public function show($id)
    {
        // return DB::select('SELECT * FROM PATIENTS WHERE PATIENTS.id = $id');
        return Patient::find($id);
    }

    public function update(Request $request)
    {

        $patient = Patient::find($request->input("id"));
        $patient->user_email = $request->input("user_email");
        $patient->services = $request->input("services");
        $patient->gdpr = $request->input("gdpr");
        $patient->save();
        
        return response()->json([
            'message' => 'patient updated!',
            'patient' => $patient
        ]);
    }



}
